# PCFanUtils - Generic Interface to PC Fans #

The **PCFanUtils** project is an attempt to provide a generic method of interfacing with common _"PC fans"_ that provide
tachometer data and/or **PWM** speed control.


## Description ##

As stated above, this project is intended to provide a _"generic" method_ of interfacing _"PC fans"_ to microcontroller
hardware; initially-targeted hardware will be **Arduino** compatible platforms, and perhaps other platforms that share
the commalities of the **Arduino** platform, such as **Energia** _(**Texas Instruments** adaptation of the **Arduino**
platform for **MSP430** microcontrollers)_.

The _common "PC fans"_ that will be supported are the **3-wire** _(provides tachometer output)_ fans, and the **4-wire**
_(provides tachometer output **and** PWM speed control)_ types. While most _"PC fans"_ operate at **12V**, **much higher**
than the **3.3V** or **5V** that modern microcontrollers operate at, this is not a problem; the tachometer output signal
is an _"open-drain"_ signal, and the **3.3V** to **5V** level of the **PWM** signal should be adequate to control the fan.
The fans will require the **12V** supply to power their motors; the **3.3V/5V** to power the microcontroller _could_ be
derived from this _with proper care_, as the fan's motor will generate quite a bit of electrical noise on the power rail.
Additionally, **5V** _"PC fans"_ are known to exist _(their use is particularly common in laptops)_, as well as fans that
require **24V** or **48V** _(the **48V** variants are typically used in **telecom** applications)_. Regardless of the fan
voltage, it is recommended to power the fans separately from the microcontroller. Additional details on wiring will be
added later. _It's always a good idea to check voltage levels of any signal entering a microcontroller with an instrument,
such as an **oscilliscope**, to reduce the risk of damaging the microcontroller!!_

Generic interfaces are provided for reading the fan's speed, both in **RPM** and as a _relative_ percentage of the fan's
**minimum** and **maximum** _observed_ **RPM** values. Additional interfaces are provided for the **PWM** speed control
of **4-wire** fans.



## Requirements ##

- Currently, the tachometer is read using an external interrupt; an **external interrupt pin** must be available for
  the tachometer.
- **PWM** output is _(obviously)_ necessary for **PWM** speed control
- A **hardware timer** is preferred for the automatic update of data
